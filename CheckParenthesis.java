
/**
* CheckParenthesis
*
*		Given a string consists of open and close parenthesis, 
*		check whether the string has balanced parenthesis.
*		Example:
*			For example, " ()" and "(()()(()))" are balanced but ")()" and "(()))()" are not.  
*		Assumption:
*			The only characters in the string are: ()					
*		
*	@author		Shon Seet
*	@since		05-18-2014
*/

public class CheckParenthesis{ 
	
	/**
	*	This method reads the input string passed in on command line
	* and parse the string and check whether parenthesis are balanced,
	*	that is, every open parenthesis should have a matching close parenthesis.
	*	@param 			str			This is the input string to check.
	*	@return							True is parenthesis are balanced, otherwise False.		
	*/
	public static boolean checkParenthesis(String str) {
	
		if (str.isEmpty()) {
			return true;									// If string is empty, returns true.
		}
		
		int openParen = 0;							// count the number of open parenthesis
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);			// get a char from the string
			if (ch == '(') {
				openParen++;			 					// increment open parenthesis count
			} else {
				// must be one if the close parenthesis
				if (openParen == 0) {
					return false;							// no prior open parenthesis, no match
				}
				openParen--;								// matching parenthesis
			}
		}
		return (openParen == 0);
	}

	/**
	* This is the main method calls checkParenthesis method.
	* @params		args	Input string with parenthesis.
	*	@return		Nothing.
	*/
	public static void main(String[] args) {
		// Check whether anything pass in on command line
		if (args.length < 1) {
			// If nothing,	print the usage of this program:
			// 			ProgramName inputString
			CheckParenthesis thisProgram = new CheckParenthesis();
			String myProgramName = thisProgram.getClass().getName();
			System.err.println("Usage:");
			System.err.println(myProgramName + " inputString");
		} else {
			// First parameter in command line is the input string
			Boolean result = checkParenthesis(args[0]);
			System.out.println("String: '" + args[0] + "' " + (result ? " has " : " does not have ") + "balanced parenthesis");
				
		}
	}
		
}

