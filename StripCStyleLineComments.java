
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.String;

/**
* StripCStyleLineComments
*	 
*		Strip C style comments from JSON string. The following is an
* 	example string:
*	
*			// this is a comment
*			{ // another comment
*				true, "foo", // 3rd comment
*				"http://www.ariba.com" // comment after URL
*			}
*
*		It reads from an input file passed in from command line.
*		It strips comments and prints output on the screen.
*
*		For each line:
*			1) Comment starts at the beginning of line
*			2) Comment starts in the middle of line
*			3) Comment "//" could be embedded in quotes(single or double)
*			4) Comment "//" could be spart of a url, as in "http://..."
*		
*	@author		Shon Seet
*	@since		05-18-2014
*/

public class StripCStyleLineComments { 

	/**
	*	This method strips comments from the string passed in
	*	@param 	inStr		This is the input string to strip comments.
	*	@return					Nothing
	*/	
	public static void stripComments(String inStr) {
		
			// This is the regular expression use to strip the comments
			String regex = "(?m)^((?:(?!//|['\"]).|['\"](?:['\"]['\"]|[^'\"])*['\"])*)//.*$";
			/**
			*	Break down of the regex that strips the comment in JSON file:
	    *
			*	(?m)							- read multiline data, but match each line - multi-line mode enabled
			*	^									-	match the start of line
			*	(									- 1 - start subpattern
			*		(?:  						- 2 - start non-capturing group, don't use it for back reference
			*			(?!//|['\"]).	- negative lookahead - if no // or single quote or double quote ahead, 
			*											match any char, except end-of-line
			*			|							- OR
			*			['\"]					- match single or double quotes
			*				(?:['\"]['\"]|[^'\"])* - match literal string
			*			['\"]					- match ending single or double quotes
			*		)*							- 2 - end of non-capturing group, repeat zero or more times
			*	)									- 1 - end subpattern
			*	//.* 							- matching comments at the end of each line to be stripped
			*	$									- match to the end of each line
			*/

			String outStr = inStr.replaceAll(regex, "$1");
			System.out.println(outStr);
	}
	
	
	/**
	*	This method reads the input file passed in on command line
	* and calls StripComments method.
	*	@param 			inFile			This is the input file to read.
	*	@return									Nothing.
	*	@exception	IOException	Exception on input error.			
	*/
	public static void readFile(String inFile) {
		try {
			// Open the input file
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			String line;
					
			// Read the contents of the file line by line
			while ((line = in.readLine()) != null) {
				stripComments(line);	
			}
					
			// Done - close the input stream
			in.close();
						
		} catch (Exception e) {
			// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	/**
	* This is the main method calls ReadFile method.
	* @params		args	Input file to strip comments.
	*	@return		Nothing.
	*	@exception
	*/
	public static void main(String[] args) {
		// Check whether anything pass in on command line
		if (args.length < 1) {
			// If nothing,	print the usage of this program:
			// 			ProgramName inputFileName
			StripCStyleLineComments thisProgram = new StripCStyleLineComments();
			String myProgramName = thisProgram.getClass().getName();
			System.err.println("Usage:");
			System.err.println(myProgramName + " inputFileName");
		} else {
			// First parameter in command line is the input file name
			readFile(args[0]);
		}
	}
		
}

