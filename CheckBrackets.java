
import java.util.*;

/**
* CheckBrackets
*
*		Given a string consists of different types of brackets, 
*		check whether the string has balanced brackets.
*		Example:
*			For example, " ([])" and "[]{}" are balanced but "([)]" and "](){" are not.  
*		Assumption:
*			The only characters in the string are: ()[]{}					
*		
*	@author		Shon Seet
*	@since		05-18-2014
*/

public class CheckBrackets { 
	
	/**
	*	This method reads the input string passed in on command line
	* and parse the string and check whether brackets are balanced,
	*	that is, every open bracket should be followed by a close bracket.
	*	@param 			str			This is the input string to check.
	*	@return							True is brackets are balanced, otherwise False.		
	*/
	public static boolean checkBrackets(String str) {
	
		if (str.isEmpty()) {
			return true;									// If string is empty, returns true.
		}
		
		Stack<Character> stack = new Stack<Character>();		// for storing all open brackets
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);			// get a char from the string
			if (ch == '(' || ch == '[' || ch == '{') {
				stack.push(ch);			 				// push open bracket into stack
			} else {
				// must be one if the close bracket
				if (stack.isEmpty()) {
					return false;							// stack is empty, but we have a close bracket
				}
				
				char last = stack.peek();		 				//get the last character from stack
				if ((last == '(' && ch == ')') || 	// check for () pair
						(last == '[' && ch == ']') || 	// check for [] pair
						(last == '{' && ch == '}')) {		// check for {} pair
							stack.pop();								  // close matches with the open bracket in stack, 
																						// remove from stack
				} else {
					return false;											// no open bracket found
				}
			}
		}
		return stack.isEmpty();
	}

	/**
	* This is the main method calls checkBrackets method.
	* @params		args	Input string with brackets.
	*	@return		Nothing.
	*/
	public static void main(String[] args) {
		// Check whether anything pass in on command line
		if (args.length < 1) {
			// If nothing,	print the usage of this program:
			// 			ProgramName inputString
			CheckBrackets thisProgram = new CheckBrackets();
			String myProgramName = thisProgram.getClass().getName();
			System.err.println("Usage:");
			System.err.println(myProgramName + " inputString");
		} else {
			// First parameter in command line is the input string
			Boolean result = checkBrackets(args[0]);
			System.out.println("String: '" + args[0] + "' " + (result ? " has " : " does not have ") + "balanced brackets");
				
		}
	}
		
}

